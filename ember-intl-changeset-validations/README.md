# ember-intl-changeset-validations

Adds support for [ember-intl](https://github.com/ember-intl/ember-intl) to [ember-changeset-validations](https://github.com/poteto/ember-changeset-validations).

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v1.4%20adopted-ff69b4.svg)](code-of-conduct.md)
[![Ember Observer Score](https://emberobserver.com/badges/ember-intl-changeset-validations.svg)](https://emberobserver.com/addons/ember-intl-changeset-validations)

## Installation

```sh
ember install ember-intl-changeset-validations
```

## Usage

In your project create a file with translations:

```json
// /translations/validations/cs-cz.json
{
  "validations": {
    "inclusion": "Musí být vybráno",
    "present": "Prosím vyplňte",
    "between": "Musí mít mezi {min} a {max} znaky",
    "invalid": "Neplatný formát",
    "email": "Nplatná e-mailová adresa"
  }
}
```

- Example is for the `cs-cz` locale.
- All validation messages have to be nested under `validations` key.
- Make sure to check the list of all the possible [default validation messages](https://github.com/poteto/ember-changeset-validations#overriding-validation-messages).

From now on all the validation messages from `ember-changeset-validation` should be translated.

You can see it in action in the test-app:
 - [/test-app/translations/validations/cs-cz.json](/test-app/translations/validations/cs-cz.json)
 - [/test-app/app/routes/application.js](/test-app/app/routes/application.js)
 - [/test-app/app/templates/application.hbs](/test-app/app/templates/application.hbs)

![Example dummy app](/test-app/public/screenshot.gif)

## Known issues

- Currently when you _change_ the locale (using [setLocale](https://ember-intl.github.io/ember-intl/docs/guide/service-api#setlocale-locale-string-array-string-void)) _after_ it has been set for the first time, then `ember-intl-changeset-validations` will **not** change the translation.

## Compatibility

* Ember.js v3.28 or above
* Ember CLI v3.28 or above
* Node.js v18 or above
* [ember-intl >= v7.x](https://ember-intl.github.io/ember-intl/versions/v7.0.6/)

## Credits

- Big thanks to [ember-i18n-changeset-validations](https://github.com/mirai-audio/ember-i18n-changeset-validations) that showed me 90% of the how-to needed to make this work.
- Kudos to [Isaac Lee](https://github.com/ijlee2) for answering questions about `ember-intl`.

## License

This project is licensed under the [MIT License](LICENSE.md).
