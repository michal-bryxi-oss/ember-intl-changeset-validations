import { module, test } from 'qunit';
import { visit, fillIn } from '@ember/test-helpers';
import { setupApplicationTest } from 'test-app/tests/helpers';
import { setupIntl, t, setLocale } from 'ember-intl/test-support';

module('Acceptance | application', function (hooks) {
  setupApplicationTest(hooks);
  setupIntl(hooks, 'cs-cz');

  hooks.beforeEach(async function () {
    await visit('/');
  });

  test('Static texts are translated', async function (assert) {
    assert.dom('#validations-blank').hasText(t('validations.blank'));
    assert.dom('#validations-url').hasText(t('validations.url'));
  });

  test('Title input validation is translated', async function (assert) {
    await fillIn('#title-input', '');
    assert.dom('#title-errors > *').exists({ count: 2 });
    assert.dom('#title-errors :nth-child(1)').hasText(t('validations.present'));
    assert
      .dom('#title-errors :nth-child(2)')
      .hasText(t('validations.tooShort', { description: 'Title', min: 1 }));

    await fillIn('#title-input', 'Titulek');
    assert.dom('#title-errors > *').doesNotExist();
  });

  test('Url input validation is translated', async function (assert) {
    await fillIn('#url-input', '');
    assert.dom('#url-errors > *').exists({ count: 3 });
    assert.dom('#url-errors :nth-child(1)').hasText(t('validations.present'));
    assert
      .dom('#url-errors :nth-child(2)')
      .hasText(
        t('validations.between', { description: 'Url', min: 11, max: 2048 }),
      );
    assert.dom('#url-errors :nth-child(3)').hasText(t('validations.url'));

    await fillIn('#url-input', 'x');
    assert.dom('#url-errors > *').exists({ count: 2 });
    await fillIn('#url-input', 'abcdefghchijklmn');
    assert.dom('#url-errors > *').exists({ count: 1 });
    await fillIn('#url-input', 'www.seznam.cz');
    assert.dom('#url-errors > *').doesNotExist();
  });

  test('Changing locale', async function (assert) {
    // While using `t` helper reacts to `setLocale`,
    // we won't get locale change for ember-intl-changeset-validations
    // This is *maybe* because of [this cache](https://github.com/poteto/ember-changeset-validations/blob/master/addon/utils/get-messages.js#L23-L45).
    const czechPresent = t('validations.present');
    const czechTitle = t('validations.tooShort', {
      description: 'Title',
      min: 1,
    });

    await fillIn('#title-input', '');

    await setLocale('cs-cz');
    assert.dom('#validations-blank').hasText(t('validations.blank'));
    assert.dom('#validations-url').hasText(t('validations.url'));
    assert.dom('#title-errors > *').exists({ count: 2 });
    assert.dom('#title-errors :nth-child(1)').hasText(czechPresent);
    assert.dom('#title-errors :nth-child(2)').hasText(czechTitle);

    await setLocale('sk-sk');
    assert.dom('#validations-blank').hasText(t('validations.blank'));
    assert.dom('#validations-url').hasText(t('validations.url'));
    assert.dom('#title-errors > *').exists({ count: 2 });
    assert.dom('#title-errors :nth-child(1)').hasText(czechPresent);
    assert.dom('#title-errors :nth-child(2)').hasText(czechTitle);
  });
});
